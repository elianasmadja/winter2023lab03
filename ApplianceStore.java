import java.util.Scanner;

public class ApplianceStore {
    public static void main(String[]args) {
        
        //creating array of dryers//
        Dryer[] ourDryers = new Dryer[4]; 
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Today we will be choosing Dryers! Have fun and pick wisely :)");
        
        //loop for user input on dryer information//
        for(int i = 0; i < ourDryers.length; i++){
            ourDryers[i] = new Dryer();
            System.out.println("Please enter the color you want for dryer  " + (i+1));
            ourDryers[i].color = scan.next();
            System.out.println("Please enter the price of purchase for dryer " + (i+1));
            ourDryers[i].price = scan.nextDouble();
            System.out.println("Please enter the size you want (ex: medium, XXL, mini) for dryer " + (i+1));
            ourDryers[i].size = scan.next();
            System.out.println("Please enter the amount of minutes it takes for your dryer " + (i+1) + " to dry");
            ourDryers[i].minsUntilDry = scan.nextInt(); 
        }
        //prints information about 4th (last) dryer inputted by user//
        System.out.println("The color of your last dryer is " + ourDryers[3].color);
        System.out.println("The price of your last dryer is " + ourDryers[3].price + "$");
        System.out.println("The size of your last dryer is " + ourDryers[3].size);
        System.out.println("The mins until dry of your last dryer is " + ourDryers[3].minsUntilDry + " minutes"); 
        
        //prints actions that 1st dryer will do//
        System.out.println("Your first dryer will complete the following actions :)");
        Dryer appliance = new Dryer();
        appliance.dryingProcess(ourDryers[0].minsUntilDry);
        appliance.allDoneRingtone();
        
    }
    
    
}