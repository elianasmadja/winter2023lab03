public class Dryer {
    //different characteristics for our dryers//
    public String color;
    public double price;
    public String size;
    public int minsUntilDry;
    
    
     //prints dring process countdown//
    public void dryingProcess(int mins) {
        for(int i = mins; i <= mins && i > 0; i--) {
            System.out.println(i + " minutes until dry");
        }
        System.out.println("Done!");
    }
    
    //prints dryer finished ringtone//
    public void allDoneRingtone(){
        System.out.println("Your clothes have been succesfully dried!");
        System.out.println("Dooo-doo-dooooo  doo-doooooo-dooooo");
    }
}

